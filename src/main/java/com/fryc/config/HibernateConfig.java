package com.fryc.config;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fryc.entities.Item;
import com.fryc.models.ItemService;
import com.fryc.repos.ItemRepo;

//@Configuration
//@EnableWebMvc
@ComponentScan(basePackages = "com.fryc")
public class HibernateConfig {

	
}