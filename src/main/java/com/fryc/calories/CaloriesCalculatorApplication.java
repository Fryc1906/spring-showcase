package com.fryc.calories;

import java.util.Arrays;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import com.fryc.entities.Item;
import com.fryc.models.ItemService;
import com.fryc.repos.ItemRepo;

@SpringBootApplication
@ComponentScan(basePackages="com.fryc")
public class CaloriesCalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaloriesCalculatorApplication.class, args);
	}
	
	@Bean
	public LocalSessionFactoryBean sessionFactory(DataSource dataSource){
		LocalSessionFactoryBean sfb = new LocalSessionFactoryBean();
		sfb.setDataSource(dataSource);
		sfb.setAnnotatedClasses(Item.class);
		sfb.setPackagesToScan(new String[] {"com.fryc.entities"});
		return sfb;
	}
	
	@Bean
	public ItemService itemService(){
		ItemService service = new ItemService();
		return service;
	}
	
	@Bean 
	public ItemRepo itemRepo(){
		ItemRepo repo = new ItemRepo();
		return repo;
	}
	
	@Bean 
	public HibernateTransactionManager hibernateTransactionManager(SessionFactory session){
		HibernateTransactionManager manager = new HibernateTransactionManager(session);
		return manager;
	}
	
	 @Bean
	    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
	        return args -> {

	            System.out.println("Let's inspect the beans provided by Spring Boot:");

	            String[] beanNames = ctx.getBeanDefinitionNames();
	            Arrays.sort(beanNames);
	            for (String beanName : beanNames) {
	                System.out.println(beanName);
	            }

	        };
	    }
}
