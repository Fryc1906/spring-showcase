package com.fryc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fryc.entities.Item;
import com.fryc.models.ItemService;

@RestController
@RequestMapping("/home")
public class ItemController {
	
	@Autowired
	private ItemService service;
	
	@RequestMapping(value="/item/{id}",method=RequestMethod.GET,produces="application/json;charset=UTF-8")
	public Item getItem(@PathVariable int id){
		return service.getItemById(id);
	}
	
	@RequestMapping(value="/items", method=RequestMethod.POST,consumes="application/json;charset=UTF-8")
	public Item addItem(@RequestBody Item item){
		return service.addItem(item);
	}
	
	
	@RequestMapping(value="/test")
	public String test(){
		return "Hello World";
	}
	
}
