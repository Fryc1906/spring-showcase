package com.fryc.repos;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fryc.entities.Item;

@Repository
public class ItemRepo {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSession(SessionFactory session){
		this.sessionFactory=session;
	}
	
	@Transactional
	public boolean add(Item item){
		boolean result = false;
		
		try{
			sessionFactory.getCurrentSession().save(item);
		}
		catch (Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public Item getById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Item item = session.get(Item.class, id);
		return item;
}
}
