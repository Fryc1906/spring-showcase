package com.fryc.models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fryc.entities.Item;
import com.fryc.repos.ItemRepo;

@Service
public class ItemService {

	@Autowired
	private ItemRepo repo;
	
	public void setItemRepo(ItemRepo repo) {
		this.repo = repo;
	}
	public Item getItemById(int id){
		return repo.getById(id);
	}
	
	public Item addItem(Item item){
		if(repo.add(item)){
			return item;
		}
		else 
			return null;
	}
}
